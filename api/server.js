'use strict';

const express = require('express');
const app     = express();
const PORT    = 8000;
const { Client, Pool } = require('pg');
const env = require('dotenv').config().parsed;
const https = require('https');


app.locals.client = new Client({
  host: env.db_host,
  database: env.db_name,
  password: env.db_password,
  user: env.db_user,
  port: env.db_port,
});
app.locals.client.connect();

// implement your gbooks functionality and route(s) here!

app.listen(PORT);

console.log("Listening on port:", PORT);

app.get('/test', (req, res) => {
  res.send({message: 'Hello World!'})
})

app.get('/dbTest', (req, res) => {
  req.app.locals.client.query(`SELECT * FROM "BookQueries"`).then(data => {
    res.send(data.rows);
  });
})

app.get('/bookSearch', (req, res) => {
  const searchTerms = req.query;
  let first = true,
      bookAPIUrl = `https://www.googleapis.com/books/v1/volumes?key=${env.google_api_key}`,
      searchTermsURL = '';

  // Builds URL params for API call
  for (let key in searchTerms) {
    let searchTerm = searchTerms[key];
    if (searchTerm.length==0) {
      continue;
    }
    searchTerm = searchTerm.replace(' ','+');

    if (first === true) {
      searchTermsURL += `&q=${key}:${searchTerm}`;
      first = false;
    } else {
      searchTermsURL += `+${key}:${searchTerm}`;
    }

    console.log(`New BookAPI URL: ${bookAPIUrl}${searchTermsURL}`);
  };

  let queryString = `SELECT * FROM "BookQueries" WHERE "query" = '${searchTermsURL}'`;

  req.app.locals.client.query(queryString, (dbError, dbResponse) => {
    if (dbError) {
      console.error(dbError);
    } else {
      // console.log(response.rows);
      if (dbResponse.rows.length === 0) {
        let fullURL = bookAPIUrl + searchTermsURL;
        callGoogleAPI(req, res, fullURL, searchTermsURL, searchTerms);
      } else {
        console.log(`Existing querie found with ID ${dbResponse.rows[0].id}`);
        let bookData = JSON.parse(dbResponse.rows[0].book_data);
        res.send(bookData);
      }
    }
  });
})

function callGoogleAPI(req, res, fullURL, searchTermsURL, searchTerms) {
  let bookAPIResponse = [];

  https.get(fullURL, response => {
    response.on('data', chunk => {
      bookAPIResponse.push(chunk);
    });

    response.on('end', () => {
      bookAPIResponse = JSON.parse(Buffer.concat(bookAPIResponse).toString());
      let bookData = [];

      // Creates new data object with the response from the Google Book API
      for (let key in bookAPIResponse.items) {
        let item = bookAPIResponse.items[key];
        let authorList = (item.volumeInfo.authors) ? convertList(item.volumeInfo.authors) : ['No authors found'];
        let categoryList = (item.volumeInfo.categories) ? convertList(item.volumeInfo.categories) : ['No genres found'];
        let thumbnailURL = (item.volumeInfo.imageLinks && item.volumeInfo.imageLinks.smallThumbnail) ? item.volumeInfo.imageLinks.smallThumbnail : null;

        bookData.push({
          id: item.id,
          title: item.volumeInfo.title,
          subtitle: item.volumeInfo.subtitle ? item.volumeInfo.subtitle : null,
          authors: authorList,
          publisher: item.volumeInfo.publisher ? item.volumeInfo.publisher : null,
          publishedDate: item.volumeInfo.publishedDate ? item.volumeInfo.publishedDate : null,
          industryIdentifiers: item.volumeInfo.industryIdentifiers ? item.volumeInfo.industryIdentifiers : ['No identifiers'],
          genre: categoryList,
          description: item.volumeInfo.description ? item.volumeInfo.description : null,
          thumbnailURL: thumbnailURL,
          bookLink: item.volumeInfo.canonicalVolumeLink ? item.volumeInfo.canonicalVolumeLink : null,
          meta: item
        });
      }

      // Inserts the returned data into the database for caching
      let query = {
        text: 'INSERT INTO "BookQueries"("query", "book_data", "search_title", "search_author", "search_publisher", "search_subject", "search_isbn") VALUES($1, $2, $3, $4, $5, $6, $7)',
        values: [searchTermsURL, JSON.stringify(bookData), searchTerms.intitle, searchTerms.inauthor, searchTerms.inpublisher, searchTerms.subject, searchTerms.isbn]
      }

      req.app.locals.client.query(query, (dbError, dbResponse) =>{
        if (dbError) {
          console.log(dbError.stack);
        }
      });

      res.send(bookData);
    });
  }).on('error', err => {
    console.error(`Error: ${err.message}`);
  });
}

function convertList(list) {
  let newList = []
  for (let key in list) {
    newList.push(list[key]);
  }
  newList = newList.join(', ');
  return newList;
}