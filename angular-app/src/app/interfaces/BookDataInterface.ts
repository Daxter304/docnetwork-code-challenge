export interface BookDataInterface {
    id: string,
    title: any,
    subtitle: any,
    authors: any,
    publisher: any,
    publishedDate: any,
    industryIdentifiers: any,
    genre: any,
    description: any,
    thumbnailURL: any,
    bookLink: any,
    meta: any
}