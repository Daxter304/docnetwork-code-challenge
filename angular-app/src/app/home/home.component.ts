import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { BookDataInterface } from '../interfaces/BookDataInterface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public loading: Boolean = false;
  public searchFields: any;
  public bookSearchData: BookDataInterface[] = [];
  public currentBookIDList: Array<string> = [];
  public searchForm: any;

  constructor(private http: HttpClient, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    console.log(`Loading: ${this.loading}`);

    this.searchForm = this.formBuilder.group({
      intitle: new FormControl(null, []),
      inauthor: new FormControl(null, []),
      inpublisher: new FormControl(null, []),
      subject: new FormControl(null, []),
      isbn: new FormControl(null, [])
    });
  }

  public runSearch() {
    let searchTerms = new HttpParams();

    for (let key in this.searchForm.value) {
      let searchTerm = this.searchForm.value[key];
      if (searchTerm !== null) {
        searchTerms = searchTerms.append(key, searchTerm.trim());
      }
    }

    this.http.get<Array<BookDataInterface>>('http://localhost:8000/bookSearch', {params: searchTerms}).subscribe(
      (response) => {
        this.loading = false;
        console.log(response);
        response.forEach(element => {
          if (!this.currentBookIDList.includes(element.id)) {
            this.currentBookIDList.push(element.id);
            this.bookSearchData.push(element);
          } else {
            console.error(`Book with ID ${element.id} is already in the list`);
          }
        });       
      });
  }
}
